package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"io"
	"io/ioutil"
	"log"
	"mime"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"
)

const (
	db                = "def"
	bucket            = "fs"
	expirationTime    = 30 * 24 * time.Hour
	configurationFile = "server_settings.json"
)

type Configuration struct {
	MongoUrl string `json:"mongo_url"`
	BaseUrl  string `json:"base_url"`
}

func check(err error) {
	if err != nil {
		log.Printf("[Error]: %s", err)
	}
}

type File struct {
	Name string
	Data io.Reader
}

type Resp struct {
	Url string `json:"url"`
	Md5 string `json:"md5"`
}

var Session *mgo.Session
var Config *Configuration

func parseForm(request *http.Request) *multipart.Part {
	mediaType, params, err := mime.ParseMediaType(request.Header.Get("Content-Type"))
	if err != nil {
		panic(err)
	}
	if strings.HasPrefix(mediaType, "multipart/") {
		mr := multipart.NewReader(request.Body, params["boundary"])
		part, err := mr.NextPart()
		if err != nil {
			panic(err)
		}
		return part
	} else {
		panic(errors.New("Cannot parse response"))
	}
}

func handlePostBin(resp http.ResponseWriter, req *http.Request) {
	defer func(resp *http.ResponseWriter) {
		if err := recover(); err != nil {
			log.Printf("[Error] %s", err)
			(*resp).WriteHeader(http.StatusInternalServerError)
		}
	}(&resp)
	part := parseForm(req)
	file, err := Session.DB("def").GridFS("fs").Create(part.FileName())
	check(err)
	n, err := io.Copy(file, part)
	file.Close()
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
	} else {
		log.Printf("read %d bytes from request", n)
		f, err := Session.DB("def").GridFS("fs").OpenId(file.Id())
		check(err)
		defer f.Close()
		log.Println(f.MD5())
		r := Resp{
			Url: fmt.Sprintf(
				"http://%s/%s/files/%s", Config.BaseUrl, "api/v1",
				f.Id().(bson.ObjectId).Hex(),
			),
			Md5: f.MD5(),
		}
		resp.Header().Set("Content-type", "appication/json")
		resp.WriteHeader(http.StatusCreated)
		b, err := json.Marshal(&r)
		resp.Write(b)
	}
}

func handleGet(resp http.ResponseWriter, req *http.Request) {
	defer func(resp *http.ResponseWriter) {
		if err := recover(); err != nil {
			(*resp).WriteHeader(http.StatusNotFound)
		}
	}(&resp)
	vars := mux.Vars(req)
	file, err := Session.DB("def").GridFS("fs").OpenId(bson.ObjectIdHex(vars["id"]))
	if err != nil {
		resp.WriteHeader(http.StatusNotFound)
		return
	}
	defer file.Close()
	resp.Header().Set("content-type", "application/zip")
	resp.Header().Set(
		"Content-disposition",
		"attachment; filename=\""+file.Name()+"\"",
	)
	n, err := io.Copy(resp, file)
	check(err)
	log.Printf("Wrote %d bytes to response", n)
	resp.WriteHeader(http.StatusOK)
}

func setHandlers(router *mux.Router) {
	router.HandleFunc("/api/v1/files", handlePostBin).Methods("POST")
	router.HandleFunc("/api/v1/files/{id:.+}", handleGet).Methods("GET")
}

func ensureIndexes() {
	index := mgo.Index{
		Key:         []string{"uploadDate"},
		Unique:      false,
		DropDups:    false,
		Background:  true,
		ExpireAfter: expirationTime,
	}
	Session.DB(db).GridFS(bucket).Files.EnsureIndex(index)
}

func init() {
	// Create session, load cfg, ensure indexes
	root, err := filepath.Abs(os.Args[0])
	check(err)
	Config = new(Configuration)
	file, err := os.Open(
		filepath.Join(filepath.Dir(root), configurationFile),
	)
	check(err)
	data, err := ioutil.ReadAll(file)
	check(err)
	err = json.Unmarshal(data, Config)
	check(err)
	Session, err = mgo.Dial(Config.MongoUrl)
	check(err)
	ensureIndexes()
}

func main() {
	router := mux.NewRouter()
	setHandlers(router)
	// Session.DB("def").C("")
	fmt.Printf("Starting server on %s\n\tlistening...\n", Config.BaseUrl)
	srv := &http.Server{
		Handler:      router,
		Addr:         Config.BaseUrl,
		WriteTimeout: 1500 * time.Second,
		ReadTimeout:  1500 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())

}
