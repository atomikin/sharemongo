
bin=bin

make-bin:
	mkdir -p bin

build-server: make-bin server.go
	go build -o ${bin}/shareserver server.go
	cp server_settings.json bin/

build-client: make-bin
	go build -o ${bin}/shareit client.go
	cp client_settings.json bin/

all: build-client build-server
