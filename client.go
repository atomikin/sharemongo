package main

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"unicode/utf8"
)

type Resp struct {
	Url string `json:"url"`
	Md5 string `json:"md5"`
}

type Settings struct {
	ServiceUrl string `json:"service_url"`
}

func check(err error) {
	if err != nil {
		// log.Fatal(err)
		panic(err)
	}
}
func ZipDir(path string) {

}

const (
	workers      = 1
	settingsFile = "client_settings.json"
	created      = 201
)

var settings *Settings

type Opt struct {
	Path string
}

func WriteZipped(path string, out *io.Writer) error {
	root, _ := filepath.Split(path)
	writer := zip.NewWriter(*out)
	log.Printf("Zipping \"%s\"...", path)
	filepath.Walk(path, func(path string, i os.FileInfo, err error) error {
		if stat, _ := os.Stat(path); !stat.IsDir() {
			// if !i.IsDir() {
			archFile := path[len(root):]
			log.Printf("%s -> %s", path, archFile)
			h := &zip.FileHeader{Name: archFile, Method: zip.Deflate, Flags: 0x800}
			file, err := writer.CreateHeader(h)
			if err != nil {
				log.Fatal(err)
				return err
			}
			fsFile, err := os.Open(path)
			if err != nil {
				log.Fatal(err)
				return err
			}
			_, err = io.Copy(file, fsFile)
			if err != nil {
				log.Fatal(err)
				return err
			}
			fsFile.Close()
		}
		return nil
	})
	err := writer.Close()
	if err != nil {
		return err
	}
	return nil
}

/*

 */
func prepareMultipartRequest(path string) (*http.Request, error) {
	_, file := filepath.Split(path)
	fmt.Println(file)
	fileName := file + ".zip"
	buffer := new(bytes.Buffer)
	multipart := multipart.NewWriter(buffer)
	fileToSend, err := multipart.CreateFormFile("file", fileName)
	if err != nil {
		return nil, err
	}
	fpath, err := filepath.Abs(path)
	err = WriteZipped(fpath, &fileToSend)
	if err != nil {
		return nil, err
	}
	multipart.Close()
	request, err := http.NewRequest("POST", settings.ServiceUrl, buffer)
	if err != nil {
		return nil, err
	}
	request.Header.Set("Content-Type", multipart.FormDataContentType())
	return request, nil
}

func init() {
	// load settings before starting the app
	settings = new(Settings)
	root, err := filepath.Abs(os.Args[0])
	check(err)
	file, err := os.Open(
		filepath.Join(filepath.Dir(root), settingsFile),
	)
	check(err)
	jsonData, err := ioutil.ReadAll(file)
	log.Println(filepath.Join(root, settingsFile))
	check(err)
	err = json.Unmarshal(jsonData, settings)
	check(err)
}

func main() {
	var data Resp
	if len(os.Args) <= 1 {
		log.Fatal("Not enough args")
	}
	path := os.Args[1]
	r, n := utf8.DecodeLastRuneInString(path)
	switch r {
	case '*', '|', ',':
		log.Fatalf("Illeal symbol in the path: %c", r)
	case '/', '\\':
		path = string([]byte(path)[:len(path)-n])
	}
	stat, err := os.Stat(path)
	if os.IsNotExist(err) {
		log.Fatalf("The provided data does not exist: \"%s\"", path)
	}
	if stat.IsDir() {
		log.Printf("Processing directory: \"%s\"...", path)
	} else {
		log.Printf("Processing file \"%s\"...", path)
	}
	request, err := prepareMultipartRequest(path)
	check(err)
	client := new(http.Client)
	resp, err := client.Do(request)
	check(err)
	if resp.StatusCode != created {
		log.Fatalf("Wrong status code returned %s", resp.Status)
	}
	payload, err := ioutil.ReadAll(resp.Body)
	check(err)
	err = json.Unmarshal(payload, &data)
	check(err)
	fmt.Printf("Your data is available now by the following URL:\n\t%s\n", data.Url)
}
